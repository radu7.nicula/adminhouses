from django.urls import reverse_lazy
from django.views.generic import CreateView

from user.forms import UserForm
from user.models import ExtendUser


class UserCreateView(CreateView):
    template_name = 'user/create-user.html'
    model = ExtendUser
    form_class = UserForm
    success_url = reverse_lazy('home')
