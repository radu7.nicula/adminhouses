from django.urls import path, include

from user import views

urlpatterns = [
    path('create_user/', views.UserCreateView.as_view(), name='create-user'),
]
