from django.contrib import messages
from django.core.mail import send_mail
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import CreateView

from adminhouses import settings
from contact.forms import ContactForm


def send_email(form_data, http_header):
    global remote_ip
    try:
        if http_header.get('HTTP_X_REAL_IP'):
            remote_ip = http_header.get('HTTP_X_REAL_IP')
        elif http_header.get('X-FORWARDED-FOR'):
            http_header.get('X-FORWARDED-FOR')
        else:
            remote_ip = 'We were not able to fetch IP Address of client'
    except:
        pass

    form_message = form_data.cleaned_data['message']
    subject = form_data.cleaned_data['subject']

    try:
        email = form_data.cleaned_data['email']
    except:
        email = 'INVALID EMAIL'

    message = f'''The current message was received from {email} with IP: {remote_ip}

    {form_message}
    '''

    send_mail(subject,
              message,
              from_email=settings.EMAIL_HOST_USER,
              recipient_list=[settings.ADMIN_EMAIL],
              fail_silently=False)


# def contact_view(request):
#     if request.method == 'POST':
#         form = ContactForm(request.POST)
#         if form.is_valid():
#             form.save()
#             return render(request, 'contact/success.html')
#     form = ContactForm()
#     context = {'form': form}
#     return render(request, 'contact/contact.html')


class ContactView(CreateView):
    form_class = ContactForm
    success_url = reverse_lazy('contact')
    template_name = 'contact/contact.html'

    def form_invalid(self, form):
        http_header = self.request.META
        send_email(form, http_header)
        messages.error(self.request, 'Something went wrong with your submission. Please try again.')
        return HttpResponseRedirect('')

    def form_valid(self, form):
        http_header = self.request.META
        send_email(form, http_header)
        messages.success(self.request, 'Your message was submitted successfully!')
        return super().form_valid(form)
